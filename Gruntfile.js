/**
 *	Gruntfile for building a single page app.
 */
module.exports = function (grunt) {

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-html2js');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-sass-globbing');

	var config = {

		/**
		 *	Directories
		 */
		
		source_dir: 'src',
		build_dir: 'build',
		tmp_dir: 'tmp',
		slides_dir: '<%= source_dir %>/slides',
		scss_dir: '<%= source_dir %>/scss',
		assets_dir: '<%= source_dir %>/assets',
		js_dir: '<%= source_dir %>/js',
		vendor_dir: '<%= source_dir %>/vendor',
		templates_dir: '<%= source_dir %>/templates',

		pkg: grunt.file.readJSON("package.json"),

		/** 
		 *	Task configurations
		 */

		// Concat JS to a single file.
		concat: {
			js: {
				options: {
					sourceMap: true
				},
				src: [ 
					'<%= js_dir %>/*.js',
					'<%= js_dir %>/**/*.directive.js',
					'<%= js_dir %>/**/*.service.js',
					'<%= js_dir %>/**/*.model.js',
					'<%= js_dir %>/**/*.controller.js',
					'<%= js_dir %>/**/*.js',
					'!<%= js_dir %>/**/*.spec.js'
				],
				dest: '<%= build_dir %>/code.js'
			}
		},

		// Scss, with globbing.
		sass: {
			dist: {
				files: {
					'<%= build_dir %>/css/styles.css' : '<%= scss_dir %>/main.scss'
				}
			}
		},

		sass_globbing: {
			generate_globs: {
				files: {
					'<%= scss_dir %>/_globbing-base.scss': '<%= scss_dir %>/partials/base/**/*.scss',
					'<%= scss_dir %>/_globbing-directives.scss': '<%= scss_dir %>/partials/directives/**/*.scss',
					'<%= scss_dir %>/_globbing-blocks.scss': '<%= scss_dir %>/partials/blocks/**/*.scss'
				},
				options: {
					useSingleQuotes: false
				}
			}
		},

		// Copy assets and sources to build directory.
		copy: {
			all: {
				files: [
					{
						src: [ 'assets/**/*', 'vendor/**/*' ],
						dest: '<%= build_dir %>',
						cwd: '<%= source_dir %>',
						expand: true
					}
				]
			}
		},

		// Bake HTML templates to angular template cache
		html2js: {
			options: {
				base: '<%= templates_dir %>',
				module: 'angularTemplates',
				singleModule: true,
				useStrict: true,
				htmlmin: {
					collapseBooleanAttributes: true,
					collapseWhitespace: true,
					removeComments: true
				}
			},
			templates: {
				src: ['<%= templates_dir %>/**/*.html'],
				dest: '<%= build_dir %>/templates.js'
			}
		},

		// Autoprefixer
		autoprefixer: {
			default: {
				files: {
					'<%= build_dir %>/css/styles.css' : '<%= build_dir %>/css/styles.css'
				},
				options: {
					map: true
				}
			}
		},

		// Watch task.
		watch: {
			sass: {
				files: ['<%= scss_dir %>/**/*.scss'],
				tasks: ['sass_globbing', 'sass', 'autoprefixer:default']
			},
			all: {
				files: ['<%= source_dir %>/**/*', '!<%= scss_dir %>/**/*', '!<%= js_dir %>/**/*'],
				tasks: ['copy:all']
			},
			assets: {
				files: ['<%= assets_dir %>/**/*'],
				tasks: ['copy:all']
			},
			js: {
				files: ['<%= js_dir %>/**/*.js', '!<%= js_dir %>/**/*.spec.js'],
				tasks: ['concat:js']
			},
			tpl: {
				files: ['<%= source_dir %>/templates/**/*.html'],
				tasks: ['html2js']
			},
			slides: {
				files: ['<%= slides_dir %>/**/*.html'],
				tasks: ['index']
			},
			src_templates: {
				files: ['<%= source_dir %>/index.html', '<%= source_dir %>/slide-template.html'],
				tasks: ['index']
			},
			json: {
				files: ['<%= js_dir %>/**/*.json', '<%= slides_dir %>/**/*.json'],
				tasks: ['index']
			}
		}
	};

	grunt.initConfig(config);

	grunt.registerTask('build', ['sass_globbing', 'sass', 'autoprefixer:default', 'copy:all', 'concat:js', 'html2js:templates', 'index']);
	grunt.registerTask('default', ['build', 'watch']);

	grunt.registerTask('index', 'Process index.html template', processIndexTemplate);


	// Read a slide HTML file
	function readSlideFile(filename) {
		var slideFile = grunt.file.read(grunt.config.get('slides_dir') + '/' + filename);
		return slideFile;
	}

	// Process the index.html template to produce build/index.html.
	// Reads the presentation.json and feeds the src/index.html template
	// with those values. The template forms the standard menu structure and
	// creates sections for Reveal.js.
	function processIndexTemplate() {
		var indexTemplate = grunt.file.read(grunt.config.get('source_dir') + '/index.html');
		var slideTemplate = grunt.file.read(grunt.config.get('source_dir') + '/slide-template.html');
		var demo = grunt.file.readJSON(grunt.config.get('slides_dir') + '/presentation.json');

		var templateInput = {
			demo: demo,
			section: function(slide, index, subIndex) {
				return grunt.template.process(slideTemplate, {data: {
					slide: slide,
					index: index,
					subIndex: subIndex,
					section: function(slide, index, subIndex) {
						return grunt.template.process(readSlideFile(slide.file), {data: {
							slide: slide,
							index: index,
							subIndex: subIndex
						}})
					}
				}});
			}

		};
		var processedIndex = grunt.template.process(indexTemplate, {data: templateInput});
		grunt.file.write(grunt.config.get('build_dir') + '/index.html', processedIndex);
	}
};