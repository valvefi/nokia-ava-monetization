

# Nokia MWC 2016 Single Page App Framework #

Heavily relies on Reveal.js.

To get it up and running:

+ Make sure Node.js is installed
+ Make sure Grunt is installed
+ Make sure Bower is installed

Ok then...

	npm install
	bower install
	grunt
	
The output will be in the __build/__ folder. __grunt__ will build and set up watch, __grunt build__ will just build the project without starting the watcher.

## How it works ##

The slides are listed in __src/slides/__ folder.

Add your slides to __Presentation.json__. Example structure is provided. You can disable slides by setting _enabled: false_.


## SCSS ##

SCSS files are under __src/scss/__ and you'll mostly be editing and adding stuff under __blocks/__.


## JS ##

JS files are under __src/js/__. AngularJS is supported and used for the menu, feel free to create controllers for whatever you need.

