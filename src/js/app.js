/**
 * The main app module declaration.
 */
angular.module("app",  [
	'ngAnimate',
	'angularTemplates',
	'rzModule'
]);
