/**
 * App configuration.
 * Global app configuration including routes is set here.
 */
(function() {
	var app = angular.module("app");

	app.$inject = ['$timeout'];
	app.run(function($timeout) {
		$timeout(function() {
			initializeRevealJS();
		});
	});

	function initializeRevealJS() {
		var options = {
			controls: false,
			progress: false,
			history: true,
			center: false,
			viewDistance: 5,
			// The "normal" size of the presentation, aspect ratio will be preserved
			// when the presentation is scaled to fit different resolutions. Can be
			// specified using percentage units.
			width: 2731,
			height: 1536,

			// Factor of the display size that should remain empty around the content
			margin: 0,

			// Bounds for smallest/largest possible scale to apply to content
			minScale: 0.2,
			// slideNumber: 'c',
			theme: Reveal.getQueryHash().theme, // available themes are in /css/theme
			//transition: Reveal.getQueryHash().transition || 'none', // default/cube/page/concave/zoom/linear/fade/none
			transition: 'none',
			backgroundTransition: 'none',
			keyboard: false,
			touch: true
		};

		Reveal.initialize(options);
		Reveal.getPreviousSlideState = function(){
			return Reveal.previousSlideState;
		};
	}
})();

