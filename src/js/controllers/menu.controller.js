(function () {
	'use strict';

	angular
		.module('app')
		.controller('MenuController', MenuController);

	MenuController.$inject = ['$scope', '$rootScope', '$timeout'];

	/* @ngInject */
	function MenuController($scope, $rootScope, $timeout) {
		var vm = this;
		vm.title = 'MenuController';
		vm.menuVisible = false;
		vm.inIntroScreen = false;
		vm.navigationState = {};
		vm.indexOverlayShown = false;
		vm.isInsideCordova = false;


		document.addEventListener('deviceready', function() {
			vm.isInsideCordova = true;
			$scope.$digest();
		});

		function fadeTitle() {

			// A bit dirty but...
			var fadeouts = document.querySelectorAll('.fadeout-title');
			if(Array.isArray(fadeouts)) {
				for(var f in fadeouts) {
					angular.element(f).removeClass('fadeout-title--fade');
					$timeout(function() {
						angular.element(f).addClass('fadeout-title--fade');
					});

				}
			}
			else {
				angular.element(fadeouts).removeClass('fadeout-title--fade');
				$timeout(function() {
					angular.element(fadeouts).addClass('fadeout-title--fade');
				});
			}
		}

		Reveal.addEventListener('ready', function(event) {
			vm.inIntroScreen = Reveal.isFirstSlide();
			vm.navigationState = Reveal.availableRoutes();
			if(!$scope.$$phase) {
				$scope.$apply();
			}
		});

		Reveal.addEventListener('slidechanged', function(event) {
			vm.indexOverlayShown = false;
			vm.inIntroScreen = Reveal.isFirstSlide();
			vm.navigationState = Reveal.availableRoutes();
			vm.indexOverlayShown = false;

			fadeTitle();

			if(!$scope.$$phase) {
				$scope.$apply();
			}

		});

		vm.resetSlideState = function () {
			vm.indexOverlayShown = false;
			vm.menuVisible = false;
			$rootScope.$broadcast('/videoSlide/reset');
			fadeTitle();
		};

		vm.goNextSlide = function() {
			Reveal.navigateRight();
		};

		vm.goPrevSlide = function() {
			Reveal.navigateLeft();
			
		};

		vm.goNextSubSlide = function() {
			Reveal.navigateDown();
		};

		vm.goPrevSubSlide = function() {
			Reveal.navigateUp();
		};

		vm.showIndexOverlay = function(show) {
			vm.indexOverlayShown = show;
		};

		vm.getIndexItems = function() {
			return window.slideSummary;
		};

		vm.isArray = function(item) {
			return Array.isArray(item);
		};

	}

})();

