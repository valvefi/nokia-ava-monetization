(function () {
	'use strict';

angular
	.module('app')
	.controller('VideoPauseOverlayExampleController', VideoPauseOverlayExample);

VideoPauseOverlayExample.$inject = ['$scope'];

/* @ngInject */
function VideoPauseOverlayExample($scope) {
	var vm = this;
	vm.title = 'VideoPauseOverlayExample';

	activate();

	////////////////

	function activate() {

	}
}

})();

