

/**
 * Fades in element when the slide it is on opens
 * 'delay' attribute can be used to delay when the element is faded in. Uses milliseconds
 */

(function () {
	'use strict';

	angular
		.module('app')
		.directive('fadein', ['$rootScope', function($rootScope){
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          if(!Reveal) {
            return
          }

          var el = element[0];
          el.style.transitionDuration = attrs.duration + 'ms';
          el.className = el.className + ' fadein';

          Reveal.addEventListener( 'slidechanged', function(evt) {
            if(!el.parentNode.className.includes('present')) {
              hideElement(el);
              return
            }

            setTimeout(function() {
              showElement(el);
            }, attrs.delay ? Number(attrs.delay) : 0)

          });
        }
      }
    }]);

    function showElement(el) {
      var prevClassNames = el.className
      el.className = prevClassNames + ' fadein--active'
    }

    function hideElement(el) {
      if(!el.className.includes('fadein--active')) {
        return
      }
      el.className = el.className.replace('fadein--active' , '')
    }

})();
