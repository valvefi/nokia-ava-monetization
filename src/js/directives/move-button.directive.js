

/**
 * Used to create buttons that can be used to navigate between slides.
 */
(function () {
	'use strict';

	angular
		.module('app')
		.directive('moveButton', ['$rootScope', function($rootScope){
      return {
        restrict: 'E',
        replace: true,
        scope: {
          label: '@',
        },
        template: '<div class="move-button" ng-bind="label"><div>',
        link: function(scope, element, attrs) {
          element.addClass(attrs.class);

          if(attrs.withHighlight) {
            element.addClass('move-button--highlight');
          }

          scope.label = attrs.label || '';

          var navigationFunction = chooseNavigationFunction(attrs.targetSlide);

          element.on('click', function() {
            if(!navigationFunction) {
              return;
            }
            navigationFunction();
          });
        }
      }
    }]);

  function chooseNavigationFunction(targetSlide) {
    if(!Reveal) {
      return null;
    }
    if(!targetSlide) {
      return null;
    }
    if(targetSlide === 'next') {
      return Reveal.navigateRight;
    }
    if(targetSlide === 'prev') {
      return Reveal.navigateLeft;
    }
    if(targetSlide.match(/^\d+$/)) { // Target slide is a number, we want to navigate to it on click
      return Reveal.slide.bind(0, parseInt(targetSlide));
    }
    return null;
  }

})();