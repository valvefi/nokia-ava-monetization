/**
 * For lazy loading.
 * Reveal.js lazy loads videos with data-src attribute set.
 * ngDataSrc produces a data-src tag out of ng-data-src tag.
 */
(function () {
	'use strict';

	angular
		.module('app')
		.directive('ngDataSrc', ngDataSrc);

	ngDataSrc.$inject = ['$interpolate'];

	/* @ngInject */
	function ngDataSrc($interpolate) {
		var directive = {
			link: link,
			restrict: 'A'
		};
		return directive;

		function link(scope, element, attrs) {
			attrs.$observe('ngDataSrc', function(newValue) {
				element[0].setAttribute('data-src', newValue);
			});
		}
	}

})();

