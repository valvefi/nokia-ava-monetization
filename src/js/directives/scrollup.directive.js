
(function () {
	'use strict';

	angular
		.module('app')
		.directive('scrollup', ['$rootScope', function($rootScope){
      return {
        restrict: 'E',
        replace: true,
        transclude: true,
        scope: {
          defaultImage: '@',
          scrollImage: '@',
          showScrollImage: '&',
          containerStyles: '@',
        },
        template: '<div class="scrollup">'
                    + '<button class="scrollup__button scrollup__button--show" ng-click="showScrollImage()"></button>'
                    + '<button class="scrollup__button scrollup__button--hide" ng-click="hideScrollImage()"></button>'
                    + '<button class="scrollup__button scrollup__button--slidein" ng-click="showSlideImage()"></button>'
                    + '<button class="scrollup__button scrollup__button--slideout" ng-click="hideSlideImage()"></button>'
                    + '<img class="default-image" src="{{defaultImageSrc}}"/>'
                    + '<div class="slide-container">'
                      + '<img class="scroll-image" src="{{scrollImageSrc}}"/>'
                      + '<img class="slide-image" src="{{slideImageSrc}}"/>'
                    + '</div>'
                    + '<ng-transclude></ng-transclude>'
                + '<div>',
        link: function(scope, element, attrs) {
          var el = element[0];
          var scrollImage = el.querySelector('.scroll-image');
          var defaultImage = el.querySelector('.default-image');
          var slideContainer = el.querySelector('.slide-container');

          element.addClass(attrs.class);

          scope.defaultImageSrc = attrs.defaultImage;
          scope.scrollImageSrc = attrs.scrollImage;
          scope.slideImageSrc = attrs.slideImage;
          scope.showScrollImage = showScrollImage;
          scope.hideScrollImage = hideScrollImage;
          scope.showSlideImage = showSlideImage;
          scope.hideSlideImage = hideSlideImage;

          Reveal.addEventListener( 'slidechanged', function(evt) {
            if(!el.parentNode.className.includes('present')) {
              hideScrollImage();
            }
          });

          function showScrollImage() {
            var defaultImageRect = defaultImage.getBoundingClientRect();
            var scrollImageRect = scrollImage.getBoundingClientRect();
            var scrollImageRelativeHeight = scrollImageRect.height / defaultImageRect.height * 100;
            
            el.style.transform = 'translate3d(0,-' + String(scrollImageRelativeHeight) + '%,0)';
          }

          function hideScrollImage() {
            el.style.transform = 'translate3d(0, 0, 0)';
          }

          function showSlideImage() {
            slideContainer.style.transform = 'translate3d(-100%, 0, 0)';
          }

          function hideSlideImage() {
            slideContainer.style.transform = 'translate3d(0, 0, 0)';
          }
        }
      }

    }]);

})();
