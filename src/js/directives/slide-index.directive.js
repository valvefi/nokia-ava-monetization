(function () {
	'use strict';

	angular
		.module('app')
		.directive('slideIndex', slideIndex);

	slideIndex.$inject = ['$timeout'];

	/* @ngInject */
	function slideIndex($timeout) {
		var directive = {
			bindToController: true,
			controller: SlideIndexController,
			controllerAs: 'vm',
			link: link,
			restrict: 'A'
		};
		return directive;

		function link(scope, element, attrs) {


			if(attrs.slideIndexInit !== undefined) {
				scope.$watch(attrs.slideIndexInit, function(val) {
					if(val) {
						$timeout(function() {
							var mason = new Masonry(element[0], {
								itemSelector: '.index-item-container',
								columnWidth: 743,
								gutter: 100,
								transitionDuration: 0
							});
						});
					}
				});
			}
			else {
				var mason = new Masonry(element[0], {
					itemSelector: '.index-item-container',
					columnWidth: 743,
					gutter: 100,
					transitionDuration: 0,
					initLayout: true
				});
			}

		}
	}

	SlideIndexController.$inject = [];

	/* @ngInject */
	function SlideIndexController() {

	}

})();


