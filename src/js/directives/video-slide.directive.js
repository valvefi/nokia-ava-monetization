/**
 * A video slide directive.
 * Produces a full screen video element. Can also pause the video when wanted and show additional templates.
 *
 * Example usages:
 * <video-slide src="assets/test.mp4"></video-slide>
 * <video-slide src="assets/test.mp4" pause-at="[5, 10]"></video-slide>
 * <video-slide src="assets/test.mp4" pause-at="[2.5, 10, 20]" pause-templates="['overlay1.html', null, 'overlay2.html']"></video-slide>
 *
 * Attributes:
 * pause-at:            An array of pause time points on the video timeline. When the video reaches the given time, it automatically pauses.
 * pause-templates:     If you want to display a template(s) on top of the video when it pauses, then provide an array of templates
 *                      here. The templates are located under src/templates. ng-enter and ng-leave styles are applied when the
 *                      template is added to the DOM so animation is easy and possible.
 *                      The array must be the same length as the pause-at array. If there is no template for a pause time (you just
 *                      want the video to pause), use null in the array.
 *                      Complex templates can be used, as the templates are compiled. You can use angular inside the templates.
 * loop:                Is the video looping or not. If set to true, video continues from beginning after it ends.
 *                      NOTE: Not compatible with pause times - video never receives ended event which means the
 *                      pause times never get reset.
 */
(function () {
	'use strict';

	angular
		.module('app')
		.directive('videoSlide', videoSlide);

	videoSlide.$inject = ['$timeout', '$interval', '$rootScope'];

	/* @ngInject */
	function videoSlide($timeout, $interval, $rootScope) {
		var directive = {
			scope: {
				src: '@',
				loop: '=',
				continueToNextSlide: '='
			},
			templateUrl: 'video-slide.html',
			link: link,
			restrict: 'E',
			controller: VideoSlideController
		};


		VideoSlideController.$inject = ['$scope', '$templateCache', '$compile', '$animate', '$timeout'];

		return directive;

		/**
		 * Check if the video element is a child of the currently open slide.
		 * @param videoElem
		 * @returns {boolean}
		 */
		function isChildOfCurrentSlide(videoElem) {
			var currentSlide = Reveal.getCurrentSlide();
			var parent = videoElem.parentNode;
			while(parent != null) {
				if(parent == currentSlide) {
					return true;
				}
				parent = parent.parentNode;
			}
			return false;
		}

		/**
		 * Directive link function.
		 * @param scope
		 * @param element
		 * @param attrs
		 */
		function link(scope, element, attrs) {

			var timeCheckInterval = null;

			function timeUpdate(internalCall) {
				var time = videoElem.currentTime;
				if(Array.isArray(scope.pauseTimes)) {
					for(var i = 0; i < scope.pauseTimes.length; i++) {
						var pauseTime = scope.pauseTimes[i].time;
						var doPause = true;
						if(pauseTime.continue) {
							doPause = false;
							pauseTime = pauseTime.time;
						}
						// Going past a pauseTime element, pause if need and attach linked template.
						if(scope.pauseTimes[i].past == false && time >= pauseTime) { // && !videoElem.paused
							if(doPause) {
								videoElem.pause();
							}
							scope.pauseTimes[i].past = true;
							scope.attachTemplate(i);
							//scope.$apply();
						}
						// Video is paused, moving back in time; remove element if we went back before its time.
						if(scope.pauseTimes[i].past && time < pauseTime && videoElem.paused) {
							scope.pauseTimes[i].past = false;
							scope.removePauseElement();
						}
					}
				}

				if(!internalCall || internalCall === undefined) {
					//scope.internalChange = true;
					scope.slider.current = time;
					scope.slider.options.ceil = videoElem.duration;
					//scope.$digest();
					//scope.internalChange = true;
				}
				else {
				}

				if(!$rootScope.$$phase) {
					scope.$digest();
				}

			}

			scope.slider = {
				current: 0,
				options: {
					floor: 0,
					ceil: 1,
					sliderScale: 1,
					onChange: function() {
						videoElem.currentTime = scope.slider.current;
						videoElem.pause();
						timeUpdate(true);
					}
				}
			};


			var videoElem = element[0].querySelector('video');

			// If not looping, show and update the slider.
			if(!scope.loop) {
				window.addEventListener('resize', function() {
					scope.slider.options.sliderScale = 1/Reveal.getScale();
					scope.$digest();

					// To be safe, sometimes resize happends a bit odd when entering/exiting fullscreen
					// and we get wrong value.
					$timeout(function() {
						scope.slider.options.sliderScale = 1/Reveal.getScale();
					}, 1000);
					//console.log(scope.slider.options.sliderScale);
				});

				videoElem.addEventListener('play', function() {
					if(timeCheckInterval != null) {
						$interval.cancel(timeCheckInterval);
					}
					timeCheckInterval = $interval(function() { timeUpdate(); }, 50);
				});

				videoElem.addEventListener('pause', function() {
					if(timeCheckInterval != null) {
						$interval.cancel(timeCheckInterval);
						timeCheckInterval = null;
					}
				});

				videoElem.addEventListener('ended', function() {
					if(timeCheckInterval != null) {
						$interval.cancel(timeCheckInterval);
						timeCheckInterval = null;
					}
				});
			}


			scope.slider.options.ceil = videoElem.duration;

			scope.directiveElement = element[0].querySelector('.video-slide');

			if(scope.loop) {
				videoElem.setAttribute('loop', 'true');
			}

			if(scope.continueToNextSlide) {
				videoElem.addEventListener('ended', function() {
					if(isChildOfCurrentSlide(videoElem)) {
						var navigationState = Reveal.availableRoutes();
						if(navigationState.down) {
							Reveal.navigateDown();
							scope.$apply();
						}
						else if(navigationState.right) {
							Reveal.navigateRight();
							scope.$apply();
						}
					}
				});
			}

			// Store pause times.
			var pauseAt = attrs['pauseAt'];
			if(pauseAt !== undefined) {
				scope.pauseTimes = scope.$eval(pauseAt);
				for(var i = 0; i < scope.pauseTimes.length; i++) {
					scope.pauseTimes[i] = {
						time: scope.pauseTimes[i],
						past: false
					};
				}
			}

			// Store pause templates.
			var pauseTemplates = attrs['pauseTemplates'];
			if(pauseTemplates !== undefined) {
				scope.pauseTemplates = scope.$eval(pauseTemplates);
			}

			// If this video element happens to be on the current slide when linking the directive
			// play it automatically.
			if(isChildOfCurrentSlide(videoElem)) {
				videoElem.currentTime = 0;
				videoElem.play();
			}

			// Same as above, executed in Reveal's onReady. Angular should be initialized before
			// Reveal to get attributes set correctly.
			Reveal.addEventListener('ready', function() {
				scope.slider.options.sliderScale = 1/Reveal.getScale();
				if(isChildOfCurrentSlide(videoElem)) {
					try {
						videoElem.currentTime = 0;
						videoElem.play();
					}
					catch(exception) {
						$timeout(function() {
							videoElem.currentTime = 0;
							videoElem.play();
						}, 800);
					}
				}
				scope.$digest();
			});

			// Listen to an external event that tells us to reset the video playback.
			scope.$on('/videoSlide/reset', function() {
				if(isChildOfCurrentSlide(videoElem)) {
					videoElem.currentTime = 0;
					videoElem.play();
				}
			});

			// On click pause/play the video.
			if(!scope.loop) {
				videoElem.addEventListener('click', function() {
					if(videoElem.paused) {
						videoElem.play();
					}
					else {
						videoElem.pause();
					}
				});
			}


			// When the video ends, reset the pause times' states.
			videoElem.addEventListener('ended', function() {
				if(Array.isArray(scope.pauseTimes)) {
					var endPause = scope.pauseTimes.indexOf('end');
					if(endPause != -1) {
						scope.attachTemplate(endPause);
					}
				}

				resetPauseTimes(scope.pauseTimes);
			});

			// Whenever we continue playing, remove the pause element.
			// Reset pause times' states when starting the video.
			videoElem.addEventListener('play', function() {
				if(videoElem.currentTime == 0) {
					resetPauseTimes(scope.pauseTimes);
				}
				scope.removePauseElement();

			});

			// On slide change pause the video, except if this video element is inside
			// the new current slide, autoplay it.
			Reveal.addEventListener('slidechanged', function(event) {
				if(!videoElem.paused && !videoElem.ended) {
					videoElem.pause();
					videoElem.currentTime = 0;
					scope.removePauseElement();
				}
				else if(isChildOfCurrentSlide(videoElem)) {
					videoElem.currentTime = 0;
					videoElem.play();
				}

			});
		}

		/**
		 *Reset the pause times' states.
		 */
		function resetPauseTimes(pauseTimes) {
			if(Array.isArray(pauseTimes)) {
				for(var i = 0; i < pauseTimes.length; i++) {
					pauseTimes[i].past = false;
				}
			}
		}

		/**
		 * The directive controller.
		 * @param $scope
		 * @param $templateCache
		 * @param $compile
		 * @param $animate
		 * @constructor
		 */
		function VideoSlideController($scope, $templateCache, $compile, $animate) {

			// Remove pause element from DOM and trigger the CSS animations.
			$scope.removePauseElement = function() {
				if($scope.pauseElement !== undefined) {
					if($scope.pauseElement.parentNode == $scope.directiveElement) {
						$animate.leave($scope.pauseElement, function() {
							$scope.directiveElement.removeChild($scope.pauseElement);
							delete $scope.pauseElement;
						});
						$scope.$digest();
					}
				}
			};

			// Attach a template to the DOM and trigger the CSS animations.
			$scope.attachTemplate = function(i) {
				if(i >= $scope.pauseTemplates.length || $scope.pauseTemplates[i] == null) {
					return;
				}

				var templateFilename = $scope.pauseTemplates[i];
				var template = $templateCache.get(templateFilename);
				var compiled = $compile(template)($scope);
				$scope.pauseElement = compiled[0];
				$scope.directiveElement.appendChild($scope.pauseElement);

				$animate.enter($scope.pauseElement, $scope.directiveElement);
			};
		}
	}
})();

